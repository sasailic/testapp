package com.click4weather.android.data;

import com.click4weather.android.model.Poi;

import java.util.HashMap;

public class WeatherData {
    private static WeatherData ourInstance = new WeatherData();

    public static WeatherData getInstance() {
        return ourInstance;
    }

    private WeatherData() {
    }

    private HashMap<String, Poi> pois = new HashMap<>();

    public void addPoi(Poi poi){
        pois.put(poi.getId(), poi);
    }

    public Poi getPoi(String id){
        return pois.get(id);
    }

    public Poi removePoi(String id){
        return pois.remove(id);
    }
}
