package com.click4weather.android.model;

import com.click4weather.android.model.world_weather_online.Data;
import com.google.android.gms.maps.model.LatLng;

import java.util.UUID;

public class Poi {
    private String id = UUID.randomUUID().toString();
    private Data weatherData;
    private LatLng location;

    public String getId() {
        return id;
    }

    public Data getWeatherData() {
        return weatherData;
    }

    public void setWeatherData(Data weatherData) {
        this.weatherData = weatherData;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }
}
