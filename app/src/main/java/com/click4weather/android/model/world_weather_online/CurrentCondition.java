
package com.click4weather.android.model.world_weather_online;

import android.content.Context;

import com.click4weather.android.R;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class CurrentCondition {

    private int FeelsLikeC;
    private int FeelsLikeF;
    private String observationTime;
    private int pressure;
    @JsonProperty("temp_C")
    private int tempC;
    @JsonProperty("temp_F")
    private int tempF;
    private int weatherCode;
    private List<WeatherDesc> weatherDesc = new ArrayList<>();
    private List<WeatherIconUrl> weatherIconUrl = new ArrayList<>();

    public int getFeelsLikeC() {
        return FeelsLikeC;
    }

    public void setFeelsLikeC(int FeelsLikeC) {
        this.FeelsLikeC = FeelsLikeC;
    }

    public int getFeelsLikeF() {
        return FeelsLikeF;
    }

    public void setFeelsLikeF(int FeelsLikeF) {
        this.FeelsLikeF = FeelsLikeF;
    }

    public String getObservationTime() {
        return observationTime;
    }

    public void setObservationTime(String observationTime) {
        this.observationTime = observationTime;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public int getTempC() {
        return tempC;
    }

    public void setTempC(int tempC) {
        this.tempC = tempC;
    }

    public int getTempF() {
        return tempF;
    }

    public void setTempF(int tempF) {
        this.tempF = tempF;
    }

    public int getWeatherCode() {
        return weatherCode;
    }

    public void setWeatherCode(int weatherCode) {
        this.weatherCode = weatherCode;
    }

    public List<WeatherDesc> getWeatherDesc() {
        return weatherDesc;
    }

    public void setWeatherDesc(List<WeatherDesc> weatherDesc) {
        this.weatherDesc = weatherDesc;
    }

    public List<WeatherIconUrl> getWeatherIconUrl() {
        return weatherIconUrl;
    }

    public void setWeatherIconUrl(List<WeatherIconUrl> weatherIconUrl) {
        this.weatherIconUrl = weatherIconUrl;
    }

    public int getMapMarker() {
        if (getTempC() < -10) {
            return R.drawable.ic_pin_blue;
        } else if (getTempC() <= 10) {
            return R.drawable.ic_pin_yellow;
        } else if (getTempC() < 30) {
            return R.drawable.ic_pin_green;
        } else {
            return R.drawable.ic_pin_orange;
        }
    }

    public String getTempLabel(Context context){
        StringBuilder temp = new StringBuilder(String.valueOf(getTempC()));
        for (int i = 0; i < 4 - temp.length(); i++) {
            temp.insert(0, " ");
        }
        return context.getString(R.string.degree, temp);
    }
}
