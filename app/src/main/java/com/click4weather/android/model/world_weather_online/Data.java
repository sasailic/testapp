
package com.click4weather.android.model.world_weather_online;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @JsonProperty("current_condition")
    private List<CurrentCondition> currentCondition = new ArrayList<>();
    private List<Weather> weather = new ArrayList<>();
    @JsonProperty("nearest_area")
    private List<NearestArea> nearestArea = new ArrayList<>();

    public List<CurrentCondition> getCurrentCondition() {
        return currentCondition;
    }

    public void setCurrentCondition(List<CurrentCondition> currentCondition) {
        this.currentCondition = currentCondition;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public boolean isValid() {
        return !currentCondition.isEmpty();
    }

    public List<NearestArea> getNearestArea() {
        return nearestArea;
    }

    public void setNearestArea(List<NearestArea> nearestArea) {
        this.nearestArea = nearestArea;
    }

    public String getLocationName(){
        if (!nearestArea.isEmpty()){
            NearestArea nearestArea = this.nearestArea.get(0);
            String name = "";
            if (nearestArea == null) return name;
            if (nearestArea.getAreaName() != null && !nearestArea.getAreaName().isEmpty()){
                name = nearestArea.getAreaName().get(0).getValue();
            }
            if (nearestArea.getCountry() != null && !nearestArea.getCountry().isEmpty()){
                if (!TextUtils.isEmpty(name)){
                    name += ", ";
                }
                name += nearestArea.getCountry().get(0).getValue();
            }
            return name;
        }
        return "";
    }
}
