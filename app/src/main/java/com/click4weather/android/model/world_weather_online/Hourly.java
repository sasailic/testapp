
package com.click4weather.android.model.world_weather_online;

import java.util.ArrayList;
import java.util.List;

public class Hourly {

    private int FeelsLikeC;
    private int FeelsLikeF;
    private int tempC;
    private int tempF;
    private String time;
    private int weatherCode;
    private List<WeatherDesc> weatherDesc = new ArrayList<>();
    private List<WeatherIconUrl> weatherIconUrl = new ArrayList<>();


    public int getFeelsLikeC() {
        return FeelsLikeC;
    }

    public void setFeelsLikeC(int FeelsLikeC) {
        this.FeelsLikeC = FeelsLikeC;
    }

    public int getFeelsLikeF() {
        return FeelsLikeF;
    }

    public void setFeelsLikeF(int FeelsLikeF) {
        this.FeelsLikeF = FeelsLikeF;
    }

    public int getTempC() {
        return tempC;
    }

    public void setTempC(int tempC) {
        this.tempC = tempC;
    }

    public int getTempF() {
        return tempF;
    }

    public void setTempF(int tempF) {
        this.tempF = tempF;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getWeatherCode() {
        return weatherCode;
    }

    public void setWeatherCode(int weatherCode) {
        this.weatherCode = weatherCode;
    }

    public List<WeatherDesc> getWeatherDesc() {
        return weatherDesc;
    }

    public void setWeatherDesc(List<WeatherDesc> weatherDesc) {
        this.weatherDesc = weatherDesc;
    }

    public List<WeatherIconUrl> getWeatherIconUrl() {
        return weatherIconUrl;
    }

    public void setWeatherIconUrl(List<WeatherIconUrl> weatherIconUrl) {
        this.weatherIconUrl = weatherIconUrl;
    }
}
