package com.click4weather.android.model.world_weather_online;

import java.util.List;

public class NearestArea {
    private List<SimpleStringValue> areaName;

    private List<SimpleStringValue> country;

    public List<SimpleStringValue> getAreaName() {
        return areaName;
    }

    public void setAreaName(List<SimpleStringValue> areaName) {
        this.areaName = areaName;
    }

    public List<SimpleStringValue> getCountry() {
        return country;
    }

    public void setCountry(List<SimpleStringValue> country) {
        this.country = country;
    }

}
