
package com.click4weather.android.model.world_weather_online;

public class WeatherIconUrl {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
