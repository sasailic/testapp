
package com.click4weather.android.model.world_weather_online;

public class WeatherResponse {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
