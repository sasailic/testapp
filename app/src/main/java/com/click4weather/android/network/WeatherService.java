package com.click4weather.android.network;

import com.click4weather.android.model.world_weather_online.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface WeatherService {
    String BASE_URL = "http://api.worldweatheronline.com";
    String API_KEY = "81583cf9ae158319f67a97ca646aa";

    String PARAMETER_FORMAT_JSON = "json";
    String PARAMETER_YES = "yes";
    String PARAMETER_NO = "no";

    @GET("premium/v1/weather.ashx")
    Call<WeatherResponse> getWeather(
            @Query("key") String apiKey,
            @Query("q") String query,
            @Query("format") String format,
            @Query("num_of_days") int numOfDays,
            @Query("tp") int timeInterval,
            @Query("includelocation") String includeLocation);
}
