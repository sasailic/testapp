package com.click4weather.android.network;

import com.click4weather.android.model.world_weather_online.WeatherResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.JacksonConverterFactory;
import retrofit2.Retrofit;

public class WeatherServiceHelper {
    private static WeatherServiceHelper ourInstance = new WeatherServiceHelper();

    public static WeatherServiceHelper getInstance() {
        return ourInstance;
    }

    private WeatherService mWeatherService;

    private WeatherServiceHelper() {
    }

    private WeatherService getWeatherService() {
        if (mWeatherService == null) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(WeatherService.BASE_URL)
                    .addConverterFactory(JacksonConverterFactory.create(mapper))
                    .build();
            mWeatherService = retrofit.create(WeatherService.class);
        }
        return mWeatherService;
    }

    public Call<WeatherResponse> getWeather(LatLng location) {
        return getWeather(location, 7);
    }

    public Call<WeatherResponse> getWeather(LatLng location, int daysForecast) {
        return getWeather(location.latitude + "," + location.longitude, daysForecast);
    }

    public Call<WeatherResponse> getWeather(String query, int daysForecast) {
        return getWeatherService().getWeather(
                WeatherService.API_KEY,
                query,
                WeatherService.PARAMETER_FORMAT_JSON,
                daysForecast,
                1,
                WeatherService.PARAMETER_YES);
    }


}
