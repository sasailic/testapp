package com.click4weather.android.ui;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.click4weather.android.R;

public class BaseActivity extends AppCompatActivity{

    void onErrorFetchingData() {
        Toast.makeText(this, R.string.Oops, Toast.LENGTH_SHORT).show();
    }
}
