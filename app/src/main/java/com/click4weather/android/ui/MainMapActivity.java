package com.click4weather.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.click4weather.android.R;
import com.click4weather.android.data.WeatherData;
import com.click4weather.android.model.Poi;
import com.click4weather.android.model.world_weather_online.CurrentCondition;
import com.click4weather.android.model.world_weather_online.WeatherResponse;
import com.click4weather.android.network.WeatherServiceHelper;
import com.click4weather.android.util.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainMapActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private ArrayList<String> mAddedPois = new ArrayList<>();
    private boolean mIsLoading;

    private ProgressBar mProgressBar;

    private Call<WeatherResponse> mWeatherServiceCall;
    private HashMap<Marker, String> mMarkers = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (savedInstanceState != null ){
            ArrayList<String> pois = savedInstanceState.getStringArrayList(Constants.KEY_CLICKED_POI_LIST);
            if (pois != null) {
                mAddedPois.addAll(pois);
            }
        }
        mProgressBar = (ProgressBar) findViewById(R.id.pb_loading);
        setIsLoading(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList(Constants.KEY_CLICKED_POI_LIST, mAddedPois);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_search:
                Intent intent = new Intent(this, SearchCityActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(this);

        if (!mAddedPois.isEmpty()){
            List<String> ids = new ArrayList<>(mAddedPois);
            for (String id : ids) {
                Poi poi = WeatherData.getInstance().getPoi(id);
                if (poi != null && poi.getWeatherData().isValid()){
                    addMarker(poi);
                }
            }
        }
        setIsLoading(false);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (mIsLoading) return;
        setIsLoading(true);
        if (mMap != null){
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }
        final Poi clickedPoi = new Poi();
        clickedPoi.setLocation(latLng);

        mWeatherServiceCall = WeatherServiceHelper.getInstance().getWeather(latLng);
        mWeatherServiceCall.enqueue(new Callback<WeatherResponse>() {

            @Override
            public void onResponse(Response<WeatherResponse> response) {
                if (response.body().getData() != null && response.body().getData().isValid()) {
                    clickedPoi.setWeatherData(response.body().getData());
                    WeatherData.getInstance().addPoi(clickedPoi);
                    addMarker(clickedPoi);
                } else {
                    onErrorFetchingData();
                }
                setIsLoading(false);
            }

            @Override
            public void onFailure(Throwable t) {
                onErrorFetchingData();
                setIsLoading(false);
            }
        });
    }

    private void setIsLoading(boolean value) {
        if (mIsLoading != value) {
            mIsLoading = value;

            mProgressBar.setVisibility(value ? View.VISIBLE : View.INVISIBLE);

            if (!value) {
                if (mWeatherServiceCall != null && !mWeatherServiceCall.isCanceled()) {
                    mWeatherServiceCall.cancel();
                }
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (mMarkers.containsKey(marker)){
            Intent intent = new Intent(this, SearchCityActivity.class);
            intent.putExtra(Constants.KEY_CLICKED_MARKER_POI_ID, mMarkers.get(marker));
            startActivity(intent);
            return true;
        }
        return false;

    }

    private void addMarker(Poi poi) {
        if (mMap != null) {
            mAddedPois.add(poi.getId());
            CurrentCondition currentCondition = poi.getWeatherData().getCurrentCondition().get(0);
            IconGenerator iconGenerator = new IconGenerator(MainMapActivity.this);
            iconGenerator.setBackground(getResources().getDrawable(currentCondition.getMapMarker()));
            iconGenerator.setTextAppearance(R.style.MarkerText);
            Marker marker = mMap.addMarker(new MarkerOptions().position(poi.getLocation())
                    .icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon(
                            currentCondition.getTempLabel(MainMapActivity.this)))));
            mMarkers.put(marker, poi.getId());
        }
    }
}
