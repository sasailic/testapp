package com.click4weather.android.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.click4weather.android.R;
import com.click4weather.android.data.WeatherData;
import com.click4weather.android.model.Poi;
import com.click4weather.android.model.world_weather_online.WeatherResponse;
import com.click4weather.android.network.WeatherServiceHelper;
import com.click4weather.android.ui.adapter.WeatherForecastAdapter;
import com.click4weather.android.util.Constants;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchCityActivity extends BaseActivity {

    private static final String TAG = SearchCityActivity.class.getSimpleName();
    private View mLoading;
    private boolean mIsLoading;
    private Call<WeatherResponse> mWeatherServiceCall;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private WeatherForecastAdapter mAdapter;
    private Poi mSearchedPoi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_city);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent().hasExtra(Constants.KEY_CLICKED_MARKER_POI_ID)) {
            String poiId = getIntent().getStringExtra(Constants.KEY_CLICKED_MARKER_POI_ID);
            if (!TextUtils.isEmpty(poiId)) {
                mSearchedPoi = WeatherData.getInstance().getPoi(poiId);
            }
        }
        if (savedInstanceState != null) {
            String poiId = savedInstanceState.getString(Constants.KEY_SEARCHED_POI);
            if (!TextUtils.isEmpty(poiId)) {
                mSearchedPoi = WeatherData.getInstance().getPoi(poiId);
            }
        }

        mLoading = findViewById(R.id.loading);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new WeatherForecastAdapter(mSearchedPoi, this);
        mRecyclerView.setAdapter(mAdapter);

        updateUI();

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();
        autocompleteFragment.setFilter(typeFilter);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(final Place place) {
                if (mIsLoading) return;
                setIsLoading(true);
                mWeatherServiceCall = WeatherServiceHelper.getInstance().getWeather(place.getLatLng());
                mWeatherServiceCall.enqueue(new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Response<WeatherResponse> response) {
                        if (response.body().getData() != null && response.body().getData().isValid()) {
                            Poi poi = new Poi();
                            poi.setWeatherData(response.body().getData());
                            poi.setLocation(place.getLatLng());
                            if (mSearchedPoi != null) {
                                WeatherData.getInstance().removePoi(mSearchedPoi.getId());
                            }
                            WeatherData.getInstance().addPoi(poi);
                            loadWeatherData(poi);
                        }
                        setIsLoading(false);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        onErrorFetchingData();
                        setIsLoading(false);
                    }
                });

            }

            @Override
            public void onError(Status status) {
                onErrorFetchingData();
                setIsLoading(false);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mSearchedPoi != null) {
            outState.putString(Constants.KEY_SEARCHED_POI, mSearchedPoi.getId());
        }
        super.onSaveInstanceState(outState);
    }

    private void loadWeatherData(Poi poi) {
        mSearchedPoi = poi;
        mAdapter.setPoi(poi);
        updateUI();
    }

    private void updateUI() {
        if (mSearchedPoi != null) {
            setTitle(mSearchedPoi.getWeatherData().getLocationName());
        } else {
            setTitle(R.string.app_name);
        }
    }

    private void setIsLoading(boolean value) {
        if (mIsLoading != value) {
            mIsLoading = value;

            mLoading.setVisibility(value ? View.VISIBLE : View.INVISIBLE);

            if (!value) {
                if (mWeatherServiceCall != null && !mWeatherServiceCall.isCanceled()) {
                    mWeatherServiceCall.cancel();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
