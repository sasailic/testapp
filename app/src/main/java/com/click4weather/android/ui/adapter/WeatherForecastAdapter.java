package com.click4weather.android.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.click4weather.android.R;
import com.click4weather.android.model.Poi;
import com.click4weather.android.model.world_weather_online.Weather;

public class WeatherForecastAdapter extends RecyclerView.Adapter {
    private final Context mContext;
    private Poi mPoi = null;

    public WeatherForecastAdapter(Poi mSearchedPoi, Context context) {
        mPoi = mSearchedPoi;
        mContext = context;
    }

    public void setPoi(Poi poi){
        mPoi = poi;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_weather_day, parent, false);

        ItemViewHolder vh = new ItemViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        Weather weather = mPoi.getWeatherData().getWeather().get(position);
        itemViewHolder.date.setText(weather.getDate());
        itemViewHolder.minTemp.setText(mContext.getString(R.string.min_temp_, weather.getMintempC()));
        itemViewHolder.maxTemp.setText(mContext.getString(R.string.max_temp_, weather.getMaxtempC()));
    }

    @Override
    public int getItemCount() {
        return (mPoi == null) ? 0 : mPoi.getWeatherData().getWeather().size();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView date;
        public TextView minTemp;
        public TextView maxTemp;

        public ItemViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.tv_date);
            minTemp = (TextView) view.findViewById(R.id.tv_min_temp);
            maxTemp = (TextView) view.findViewById(R.id.tv_max_temp);
        }
    }
}
