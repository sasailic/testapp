package com.click4weather.android.util;

public class Constants {
    public static final String KEY_CLICKED_POI_LIST = "KEY_CLICKED_POI_LIST";
    public static final String KEY_SEARCHED_POI = "KEY_SEARCHED_POI";
    public static final String KEY_CLICKED_MARKER_POI_ID = "KEY_CLICKED_MARKER_POI_ID";
}
